<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Models\Author;
use App\Models\Category;
use Dotenv\Util\Str;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class ArticleController extends Controller
{
    public function index()
    {
        $category = Category::all()->sortBy('id');
        $authors = Author::all()->sortBy('last_name');
        $articles = Article::with(['category', 'author'])->get();
        $articles =Article::paginate(8); 
        return view('crud/article', ['category'=>$category, 'authors'=>$authors,'articles'=>$articles]);
    }

    public function store(Request $request): RedirectResponse
    {
        if( !empty($request->image)){

            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $imageName);
        }
        $article = new Article();
        $article->name = $request->name;
        $article->text = $request->text;
        $article->category_id = $request->category;
        $article->author_id = $request->author;
        $article->image =  $imageName;
        $article->save();
        return Redirect::to('crud/article');

    }

    public function delete(Request $request): RedirectResponse
    {
        Article::where('id', $request->id)->delete();
        return Redirect::to('crud/article');
    }

    public function update(Request $request, string $id)
    {
            
        Article::where('id', '=', $id)->update([ 
            'name' => $request->name, 
            'text' => $request->text,
            'author_id' => $request->author,
            'category_id' => $request->category,
            // 'image'=> $imageName,
        ]);
        return Redirect::to('crud/article');
    }

}
