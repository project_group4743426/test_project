<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class AuthorController extends Controller
{
   
    public function index()
    {
        $authors = Author::all()->sortBy('name');
        $authors =Author::paginate(10); 
        foreach ($authors as $newAuthors)
        {
            $arrAuthorsToArticle = Article::where('author_id', '=', $newAuthors->id)->get();
            $arrCount = count($arrAuthorsToArticle);
            $idNewAuthors = $newAuthors->id;
            $nameFirstNewAuthors = $newAuthors->first_name;
            $nameLastNewAuthors = $newAuthors->last_name;
            $dateCreateNewAuthors = $newAuthors->created_at;
            $dateUpdateNewAuthors = $newAuthors->updated_at;
            $arrItemAuthors = array(
                'count' => $arrCount, 
                'id' => $idNewAuthors, 
                'first_name' => $nameFirstNewAuthors,
                'last_name' => $nameLastNewAuthors,
                'created_at' => $dateCreateNewAuthors,
                'updated_at' => $dateUpdateNewAuthors);
            $arrAuthors [] = $arrItemAuthors;
        }
        // dd($arrAuthors);

        return view('crud/author', ['authors'=>$arrAuthors]);

    }

    public function store(Request $request): RedirectResponse
    {
        
        $author = new Author();
        $author->first_name = $request->first_name;
        $author->last_name = $request->last_name;
        $author->save();
        return Redirect::to('crud/author');

    }

    public function update(Request $request, string $id)
    {
        Author::where('id', $request->id)->update([ 'first_name' => $request->first_name, 'last_name' => $request->last_name]);
        return Redirect::to('crud/author');
    }


    public function delete(Request $request): RedirectResponse
    {
        Author::where('id', $request->id)->delete();
        return Redirect::to('crud/author');
    }
}
