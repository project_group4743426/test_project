<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::all()->sortBy('id');
        $category =Category::paginate(10); 
        return view('crud/category', ['category'=>$category]);
    }

    public function store(Request $request): RedirectResponse
    {
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        return Redirect::to('crud/category');

    }
    
    public function update(Request $request): RedirectResponse
    {
        $categoryUpdate = Category::where('id', $request->id)->update([ 'name' => $request->name]);
        return Redirect::to('crud/category');

    }

    public function delete(Request $request): RedirectResponse
    {
       
        $categoryDelete = Category::where('name', $request->name)->delete();
        return Redirect::to('crud/category');
    }
}
