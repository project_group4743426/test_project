<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Article;
use App\Models\Author;

class MainPageController extends Controller
{
    public function category()
    {
        $category = Category::all()->sortBy('name');
        foreach ($category as $newCategory)
        {
            $arrArticlesTocategory = Article::where('category_id', '=', $newCategory->id)->get();
            $arrCount = count($arrArticlesTocategory);
            $idNewCategory = $newCategory->id;
            $nameNewCategory = $newCategory->name;
            $arrItemCategory = array('count' => $arrCount, 'id' => $idNewCategory, 'name' => $nameNewCategory);
            $arrCategories [] = $arrItemCategory;
        }
     
        $article = Article::with(['category', 'author'])->get();
        return view('mainpage', ['category'=>$arrCategories, 'article'=>$article]);
    }

    public function sortToCategory($category_name)
    {
        if(is_numeric($category_name))
        {
            $category = Category::all()->sortBy('name');
            foreach ($category as $newCategory)
            {
                $arrArticlesTocategory = Article::where('category_id', '=', $newCategory->id)->get();
                $arrCount = count($arrArticlesTocategory);
                $idNewCategory = $newCategory->id;
                $nameNewCategory = $newCategory->name;
                $arrItemCategory = array('count' => $arrCount, 'id' => $idNewCategory, 'name' => $nameNewCategory);
                $arrCategories [] = $arrItemCategory;
            }
        $categoryArticles =  Article::all()->where('category_id', '=', $category_name);
        return view('mainpage', ['category'=>$arrCategories, 'article'=>$categoryArticles]);
        }
        else{
            return view($category_name);
        }
    }
}
