<?php

namespace App\Http\Controllers;

use App\Http\Requests\RestRequest;
use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use Illuminate\Http\Request;


class RestController extends Controller
{

    public function index()
    {
        
        $authors = Author::all()->sortBy('name');
        $articles = Article::all()->sortBy('id');
        $category = Category::all()->sortBy('id');


        return view('data/rest', ['authors'=>$authors, 'articles'=>$articles, 'category'=>$category]);
        // return response()->json([
        //     'authors' => view('data/rest')->with('authors',$authors)->render()
        // ]);
        // return response()->json(['authors'=>$authors]);
    }

    public function listArticle($id)
    {
        // dd($id);
        $articles =  Article::all()->where('author_id', '=', $id);
        return response()->json(['authors'=>$articles]);
    }

    public function byId($id)
    {
        $articlesById = Article::all()->where('id', '=', $id);
        return response()->json(['authors'=>$articlesById]);
    }

    public function updateArticle(RestRequest $request, $id)
    {
        $article = Article::where('id', '=', $id)->update([ 
            'name' => $request->name, 
            'text' => $request->text,
            'author_id' => $request->author,
            'category_id' => $request->category,
            // 'image'=> $imageName,
        ]);
        return response()->json($article, 200);
    }

    public function delete($id)
    {
        // dd(json_decode($id));
        Article::where('id',$id)->delete();

        return 204;
    }
}
