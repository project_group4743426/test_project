<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Article;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class RestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize()
    {
        // abort_if(Gate::denies('user_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // return true;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'text' => 'required|string',
            'category_id' => 'exists:categories,id',
            'author_id' => 'exists:authors,id',
            'image' => 'image',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Название статьи обязательно',
            'text.required' => 'Текст статьи обязателен',
            'name.exists'  => 'Название статьи должно быть уникальным',
        ];
    }
}
