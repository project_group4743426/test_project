<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 1',
            'text' => 'Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...',
            'category_id' => '1',
            'author_id' => '2',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 2',
            'text' => 'Рандомный текст',
            'category_id' => '2',
            'author_id' => '2',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 3',
            'text' => 'Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...',
            'category_id' => '3',
            'author_id' => '1',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 4',
            'text' => 'Рандомный текст',
            'category_id' => '4',
            'author_id' => '1',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 5',
            'text' => 'Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...',
            'category_id' => '5',
            'author_id' => '2',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 6',
            'text' => 'Рандомный текст.',
            'category_id' => '6',
            'author_id' => '2',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 7',
            'text' => 'Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...',
            'category_id' => '7',
            'author_id' => '1',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 8',
            'text' => 'Рандомный текст',
            'category_id' => '8',
            'author_id' => '2',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('articles')->insert([
            'name' => 'Тестовая статья 9',
            'text' => 'Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...',
            'category_id' => '1',
            'author_id' => '1',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);

    }
}
