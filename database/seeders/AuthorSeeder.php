<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('authors')->insert([
            'first_name' => 'John',
            'last_name' => 'Doe',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('authors')->insert([
            'first_name' => 'Jane',
            'last_name' => 'Doe',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
    }
}
