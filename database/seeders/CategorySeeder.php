<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            'name' => 'Игры',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('categories')->insert([
            'name' => 'Здоровье',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('categories')->insert([
            'name' => 'Путешествия',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('categories')->insert([
            'name' => 'Музыка',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('categories')->insert([
            'name' => 'Досуг',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('categories')->insert([
            'name' => 'Еда',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('categories')->insert([
            'name' => 'Техника',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
        DB::table('categories')->insert([
            'name' => 'Наука',
            'created_at' =>now(),
            'updated_at' =>now(),
        ]);
    }
}
