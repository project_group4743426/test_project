<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Philosophy</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ mix('/css/base.css') }} ">
    <link rel="stylesheet" href="../css/vendor.css"> 
    <link rel="stylesheet" href="../css/main.css">

   

    <!-- script
    ================================================== -->
    <script src="../js/modernizr.js"></script>
    <script src="../js/pace.min.js"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">


</head>

<body id="top">

    <div class="header-logo-auth">
    <div class="header__logo">
        <a class="logo" href="{{ url('/') }}">
            <img src="../images/logo.svg" alt="Homepage">
        </a>
    </div> 

    <div class="hidden sm:flex sm:items-center sm:ml-6">
        <x-dropdown align="right" width="48">
            <x-slot name="trigger">
                <button style="width: 250px;" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 dark:text-gray-400 bg-white dark:bg-gray-800 hover:text-gray-700 dark:hover:text-gray-300 focus:outline-none transition ease-in-out duration-150">
                    <div>{{ Auth::user()->name }}</div>
                </button>
            </x-slot>

            <x-slot name="content">
                <x-dropdown-link :href="route('profile.edit')">
                    {{ __('Profile') }}
                </x-dropdown-link>

                <br><a href="{{ URL::previous() }}">Back</a>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-dropdown-link :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link>
                </form>
            </x-slot>
        </x-dropdown>
    </div>
</div>



<div >
    <div class="" style="display: flex; justify-content: center;">
               
        <form class="form-ar-store" action="{{ route('articles.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
              <div class="field">
                <input type="text" name="name" placeholder="Название" class="text-aliceblue">
              </div>
              <div class="field">
                <textarea type="text" name="text" placeholder="Текст статьи" class="text-aliceblue"></textarea>
              </div>
              <div class="field">
                <input type="file" name="image" placeholder="Название" class="text-aliceblue">
              </div>
              <div class="field">
                <select name="category" id="category" style="color: #6d6d6d;">
                    <option value="">Выберите категорию</option>
                    @foreach ($category as $categoryItem)
                    <option value="{{$categoryItem->id}}">{{$categoryItem->name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="field">
                    <select name="author" id="author" style="color: #6d6d6d;">
                    <option value="">Выберите автора</option>
                    @foreach ($authors as $authorsItem)
                    <option value="{{$authorsItem->id}}">{{$authorsItem->first_name}} {{$authorsItem->last_name}}</option>
                    @endforeach
                </select>
              </div>
         
              <div>
                <button type="submit">Добавить</button>
              </div>
          </form>
          
    </div> 


    <div class="row" style="margin-left: 10%;">
        <div class="col-full">
            <nav class="pgn">
                <ul>
                   
                    <div class="divTable" style="border: 2px solid #000;" >
                        <div class="divTableBody">
                        <div class="divTableRow">
                        <div class="divTableCell">Информация о статье</div>
                        </div>
                    @foreach ($articles as $articlesItem)
                    <form class="" action="{{ route('articles.update', $articlesItem->id)}}" method="POST" >
                      @csrf
                      @method('patch')
                        <div class="divTableRow">
                        <div class="divTableCell">{{$articlesItem->id}}</div>
                        <div class="divTableCell">
                         <input type="text" name="name" value="{{$articlesItem->name}}" class="text-aliceblue">
                        </div>
                        <div class="divTableCell">
                          <textarea type="text" name="text" class="text-aliceblue">{{$articlesItem->text}}</textarea>
                        </div>
                        <div class="divTableCell">
                          <select name="author" id="author" style="color: #6d6d6d;">
                            @foreach ($authors as $authorsItem)
                            <option value="{{$authorsItem->id}}"  {{ $authorsItem->id == $articlesItem->author_id ? 'selected="selected"' : '' }}>{{$authorsItem->first_name}} {{$authorsItem->last_name}}</option>
                            
                            @endforeach
                        </select>
                        </div>
                        <div class="divTableCell">
                          <select name="category" id="category" style="color: #6d6d6d;">
                            <option value="">Выберите категорию</option>
                            @foreach ($category as $categoryItem)
                            <option value="{{$categoryItem->id}}" {{ $categoryItem->id == $articlesItem->category_id ? 'selected="selected"' : '' }}>{{$categoryItem->name}}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="divTableCell">{{Carbon\Carbon::parse($articlesItem->created_at)->format('d-m-y h:i')}}</div>
                        <div class="divTableCell">{{Carbon\Carbon::parse($articlesItem->updated_at)->format('d-m-y h:i')}}</div>
                        <div class="divTableCell">
                            @if (!empty($articlesItem->image))
                                <img src="{{ asset("../images/{$articlesItem->image}")}}" alt=""> 
                            @else
                            <img src="../images/thumbs/masonry/wheel-400.jpg" 
                            srcset="../images/thumbs/masonry/wheel-400.jpg 1x, ../images/thumbs/masonry/wheel-800.jpg 2x" alt="" style="width: 225px;height: 225px;">
                            @endif
                          {{-- <input type="file" name="image" placeholder="Название" class="text-aliceblue" value="значение"> --}}
                        </div>

                        <div class="divTableCell">
                        
                          <button>Редактировать</button></form>
                          <form class="" action="{{ route('articles.delete') }}" method="POST" >
                            @csrf
                              <div class="field">
                                  <input type="text" name="id" value="{{$articlesItem->id}}" class="text-aliceblue" style="visibility: collapse;display: none;">
                                </div>
                           
                                <div>
                                  <button type="submit">Удалить</button>
                                </div>
                            </form>
                        
                        </div>

                        </div>
                    @endforeach
                        </div>
                        </div>
                        {{ $articles->links() }}
                </ul>
            </nav>
        </div>
    </div> 



    <footer class="s-footer">

        <div class="s-footer__main">
            <div class="row">

          
                
              


            </div>
        </div> 

      

    </footer> 

    <!-- Java Script
    ================================================== -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>

</html>