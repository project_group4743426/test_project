<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Philosophy</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ mix('/css/base.css') }} ">
    <link rel="stylesheet" href="../css/vendor.css"> 
    <link rel="stylesheet" href="../css/main.css">

   

    <!-- script
    ================================================== -->
    <script src="../js/modernizr.js"></script>
    <script src="../js/pace.min.js"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">


</head>

<body id="top">

    <div class="header-logo-auth">
    <div class="header__logo">
        <a class="logo" href="{{ url('/') }}">
            <img src="../images/logo.svg" alt="Homepage">
        </a>
    </div> 

    <div class="hidden sm:flex sm:items-center sm:ml-6">
        <x-dropdown align="right" width="48">
            <x-slot name="trigger">
                <button style="width: 250px;" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 dark:text-gray-400 bg-white dark:bg-gray-800 hover:text-gray-700 dark:hover:text-gray-300 focus:outline-none transition ease-in-out duration-150">
                    <div>{{ Auth::user()->name }}</div>
                </button>
            </x-slot>

            <x-slot name="content">
                <x-dropdown-link :href="route('profile.edit')">
                    {{ __('Profile') }}
                </x-dropdown-link>

                <br><a href="{{ URL::previous() }}">Back</a>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-dropdown-link :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link>
                </form>
            </x-slot>
        </x-dropdown>
    </div>
</div>

<div style="display: flex;justify-content: space-around;">
        <div class="">
                   
            <form class="" action="{{ route('category.store') }}" method="POST" >
              @csrf
                <div class="field">
                    <input type="text" name="name" placeholder="Название категории" class="text-aliceblue">
                  </div>
             
                  <div>
                    <button type="submit">Добавить</button>
                  </div>
              </form>
              
        </div> 

        <div class="">
               
            <form class="" action="{{ route('category.update', 'update')}}" method="POST" >
                @method('patch')
              @csrf
                  <div class="field">
                    <input type="text" name="id" placeholder=" Id категории" class="text-aliceblue">
                  </div>
    
                  <div class="field">
                    <input type="text" name="name" placeholder="Название категории" class="text-aliceblue">
                  </div>
             
                  <div>
                    <button type="submit">Редактировать</button>
                  </div>
              </form>
            </div> 

        <div class="">
           
            <form class="" action="{{ route('category.delete') }}" method="POST" >
              @csrf
                <div class="field">
                    <input type="text" name="name" placeholder="Название категории" class="text-aliceblue">
                  </div>
             
                  <div>
                    <button type="submit">Удалить</button>
                  </div>
              </form>
              
        </div> 


        
    </div>

        <div class="row">
            <div class="col-full">
                <nav class="pgn">
                    <ul>
                       
                        <div class="divTable" style="border: 2px solid #000;" >
                            <div class="divTableBody">
                            <div class="divTableRow">
                            <div class="divTableCell">ID</div>
                            <div class="divTableCell">НАЗВАНИЕ</div>
                            <div class="divTableCell">СОЗДАНО</div>
                            <div class="divTableCell">ОБНОВЛЕНО</div>
                            </div>
                        @foreach ($category as $categoryItem)
                        {{-- dd{{$categoryItem}}  --}}
                            <div class="divTableRow">
                            <div class="divTableCell">{{$categoryItem->id}}</div>
                            <div class="divTableCell">{{$categoryItem->name}}</div>
                            <div class="divTableCell">{{Carbon\Carbon::parse($categoryItem->created_at)->format('d-m-y h:i')}}</div>
                            <div class="divTableCell">{{Carbon\Carbon::parse($categoryItem->updated_at)->format('d-m-y h:i')}}</div>
                            </div>
                        @endforeach
                            </div>
                            </div>
                    </ul>


{{ $category->links() }}
                </nav>
            </div>
        </div> 



    <footer class="s-footer">

        <div class="s-footer__main">
            <div class="row">

          
                
              


            </div>
        </div> 

      

    </footer> 

    <!-- Java Script
    ================================================== -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>

</html>

