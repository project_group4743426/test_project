<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <link rel="stylesheet" href="{{ mix('/css/base.css') }} "> 
      <link rel="stylesheet" href="css/vendor.css"> 
      <link rel="stylesheet" href="css/main.css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <div class="header__logo">
          <a class="logo" href="{{ url('/') }}">
              <img src="images/logo.svg" alt="Homepage">
          </a>
      </div> 
        
        <title>Вход</title>
		<link href="css/app.css" rel="stylesheet">
</head>
<body>

  <div class="col-five md-full end s-footer__subscribe">
    <x-auth-session-status class="mb-4" :status="session('status')" />
    <form class="formAuth" action="{{ route('login') }}" method="POST" >
      @csrf
        <div class="field">
          
            <x-input-label for="email" :value="__('Email')" style="color: aliceblue;"/>
            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus autocomplete="username" placeholder="email"/>
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
          </div>
  
        <div class="field">
          <x-input-label for="password" :value="__('Password')" style="color: aliceblue;"/>

          <x-text-input id="password" class="block mt-1 w-full"
                          type="password"
                          name="password"
                          required autocomplete="current-password" placeholder="Пароль"/>

          <x-input-error :messages="$errors->get('password')" class="mt-2" />

        </div>
        <div>
              <label for="remember_me" class="inline-flex items-center">
              <input id="remember_me" type="checkbox" class="rounded dark:bg-gray-900 border-gray-300 dark:border-gray-700 text-indigo-600 shadow-sm focus:ring-indigo-500 dark:focus:ring-indigo-600 dark:focus:ring-offset-gray-800" name="remember">
              <span style="color: aliceblue;" class="ml-2 text-sm">{{ __('Запомнить меня') }}</span>
          </label>
          </div>
          <div>
            <button type="submit">Вход</button>
          </div>
      </form>
      
</div> 

</body>
</html>