<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Philosophy</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="css/base.css"> 
    <link rel="stylesheet" href="css/vendor.css"> 
    <link rel="stylesheet" href="css/main.css">

   

    <!-- script
    ================================================== -->
    <script src="js/modernizr.js"></script>
    <script src="js/pace.min.js"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top">
    <section class="s-pageheader s-pageheader--home">
        <header class="header">
            <div class="header__content row">
                <div class="header__logo">
                    <a class="logo" href="{{ url('/') }}">
                        <img src="images/logo.svg" alt="Homepage">
                    </a>
                </div> 
                <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>
                <nav class="header__nav-wrap">
                    <h2 class="header__nav-heading h6">Site Navigation</h2>
                    <ul class="header__nav">
                        <li class="current"><a href="{{ url('/') }}" title="">Главная</a></li>
                        
                        @if (!empty($category))
                            <li class="has-children">
                            <a href="#0" title="">Категории</a>
                            <ul class="sub-menu">
                            @foreach ($category as $item)
                            @if ($item['count']>0)
                               <li><a href="{{ url('/', ['category_name' => $item['id'] ]) }}">{{$item['name']}}({{$item['count']}})</a></li>   
                            @endif
                            @endforeach
                            </ul>
                        </li> 
                        @endif

                        @auth
                        <li><a href="{{ url('cabinet') }}" title="">Личный кабинет</a></li>
                        @else
                        <li><a href="{{ url('login') }}" title="">Вход в личный кабинет</a></li>
                        @endauth
                    </ul> 
                </nav> 
            </div> 
        </header> 
    </section> 

  
    <section class="s-content">
        <div class="row masonry-wrap">
            <div class="masonry">
                <div class="grid-sizer"></div>
                  {{-- {{ dd($article); }} --}}
                 @foreach ($article as $itemArticle)

                <article class="masonry__brick entry format-standard" data-aos="fade-up">

                    <div class="entry__thumb" style="text-align: center;">
                      @if (!empty($itemArticle->image))
                      <img src="{{ asset("../images/{$itemArticle->image}")}}" alt=""> 
                      @else
                          <img src="images/thumbs/masonry/wheel-400.jpg" 
                                    srcset="images/thumbs/masonry/wheel-400.jpg 1x, images/thumbs/masonry/wheel-800.jpg 2x" alt="">
                      @endif
                    </div>
    
                    <div class="entry__text">
                        <div class="entry__header">
                            
                            <div class="entry__date">
                                <li>{{$itemArticle->author->first_name}} {{$itemArticle->author->last_name}}</li>
                                 {{$itemArticle->created_at}}
                            </div>
                            <h1 class="entry__title"><a href="single-standard.html">{{$itemArticle->name}}</a></h1>
                            
                        </div>
                        <div class="entry__excerpt">
                            <p>
                                {{$itemArticle->text}}
                            </p>
                        </div>
                        <div class="entry__meta">
                            <span class="entry__meta-links">
                                  <a href="{{ url('/', ['category_name' => $itemArticle->category->id]) }}">{{$itemArticle->category->name}}</a>      
                            </span>
                        </div>
                    </div>
    
                </article> 
                @endforeach
        

            </div> 
        </div>

        <!-- <div class="row">
            <div class="col-full">
                <nav class="pgn">
                    <ul>
                        <li><a class="pgn__prev" href="#0">Prev</a></li>
                        <li><a class="pgn__num" href="#0">1</a></li>
                        <li><span class="pgn__num current">2</span></li>
                        <li><a class="pgn__num" href="#0">3</a></li>
                        <li><a class="pgn__num" href="#0">4</a></li>
                        <li><a class="pgn__num" href="#0">5</a></li>
                        <li><span class="pgn__num dots">…</span></li>
                        <li><a class="pgn__num" href="#0">8</a></li>
                        <li><a class="pgn__next" href="#0">Next</a></li>
                    </ul>
                </nav>
            </div>
        </div> -->

    </section>



    <footer class="s-footer">

        <div class="s-footer__main">
            <div class="row">
                
              


            </div>
        </div> 

      

    </footer> 


    <div id="preloader">
        <div id="loader">
            <div class="line-scale">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>


    <!-- Java Script
    ================================================== -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>

</html>