<?php

use App\Http\Controllers\RestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('articles/list/{id}', [RestController::class, 'listArticle'])->name('rest.list');
Route::get('articles//by-id/{id}', [RestController::class, 'byId'])->name('rest.byid');
Route::post('articles/update/{id}', [RestController::class, 'updateArticle'])->name('rest.update');
Route::get('articles/{id}', [RestController::class, 'delete'])->name('rest.delete');
