<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MainPageController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\RestController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('mainpage');
});

Route::get('/', [MainPageController::class, 'category']);
Route::get('/{category_name}', [MainPageController::class, 'sortToCategory']);

// Route::get('/cabinet', function () {
//     return view('cabinet');
// })->middleware(['auth', 'verified'])->name('cabinet');

Route::get('/cabinet')->middleware(['checkAuth']);

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::post('/articles/delete', [ArticleController::class, 'delete'])->name('articles.delete');
Route::resource('articles', ArticleController::class);
Route::post('/authors/delete', [AuthorController::class, 'delete'])->name('authors.delete');
Route::resource('authors', AuthorController::class);
Route::post('/category/delete', [CategoryController::class, 'delete'])->name('category.delete');
Route::resource('category', CategoryController::class);

Route::get('crud/category', function () {
    return view('crud/category');
});
Route::get('crud/category', [CategoryController::class, 'index']);
Route::get('profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');

Route::get('crud/author', function () {
    return view('crud/author');
});
Route::get('crud/author', [AuthorController::class, 'index']);

Route::get('crud/article', function () {
    return view('crud/article');
});
Route::get('crud/article', [ArticleController::class, 'index']);

Route::get('data/rest', function () {
    return view('data/rest');
});
Route::get('data/rest', [RestController::class, 'index']);

require __DIR__.'/auth.php';
