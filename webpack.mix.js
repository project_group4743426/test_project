const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    /* CSS */
    .copy('resources/css/base.css', 'public/css/base.css')
    .copy('resources/css/main.css', 'public/css/main.css')
    .copy('resources/js/app.js', 'public/js/app.js')
    .copy('resources/css/app.css', 'public/css/app.css')



    .version()


/* Tools */
    .browserSync('localhost:8000')
    .disableNotifications()

    /* Options */
    .options({
        processCssUrls: false
    });

